-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-04-2019 a las 17:36:25
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `curso2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `biologia`
--

CREATE TABLE `biologia` (
  `Nal` int(3) UNSIGNED ZEROFILL NOT NULL,
  `Nombre` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Apellidos` varchar(30) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Localidad` varchar(20) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'Santander',
  `Telefono` char(9) COLLATE latin1_spanish_ci NOT NULL,
  `Nota` decimal(3,1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `biologia`
--

INSERT INTO `biologia` (`Nal`, `Nombre`, `Apellidos`, `Localidad`, `Telefono`, `Nota`) VALUES
(001, 'Javier', 'Ramos', 'Torrelavega', '942807090', '8.5'),
(003, 'Rebeca ', 'Santos	', 'Santander', ' 65033445', '10.0'),
(006, 'Manuel	', 'Arrieta', 'Santander', '942656463', '4.5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `extras`
--

CREATE TABLE `extras` (
  `Nal` int(3) UNSIGNED ZEROFILL DEFAULT NULL,
  `Actividad` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `Horas` tinyint(2) NOT NULL,
  `id` int(3) UNSIGNED ZEROFILL NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `biologia`
--
ALTER TABLE `biologia`
  ADD PRIMARY KEY (`Nal`);

--
-- Indices de la tabla `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Nal` (`Nal`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `biologia`
--
ALTER TABLE `biologia`
  MODIFY `Nal` int(3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `extras`
--
ALTER TABLE `extras`
  ADD CONSTRAINT `FK_1` FOREIGN KEY (`Nal`) REFERENCES `biologia` (`Nal`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
